﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public static UIManager Instance = null;

    [SerializeField]
    List<GameObject> _hearts = new List<GameObject>();
    private void Awake()
    {
        if(Instance==null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        SetHealth(3);
    }
    public void SetHealth(int health)
    {
        for(int index=0;index<_hearts.Count;index++)
        {
            if(index<health)
            {
                _hearts[index].SetActive(true);
            }
            else
            {
                _hearts[index].SetActive(false);
            }
        }
    }
}
