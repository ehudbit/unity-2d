﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CountEnemyLogic : MonoBehaviour
{
    public int _countPoint;

    GUIStyle _guiStyle = new GUIStyle();
    public Font _font;

    private void OnGUI()
    {
        _guiStyle.fontSize = 60;
        GUI.skin.font = _font;
        _guiStyle.normal.textColor = Color.black;
        GUI.Label(new Rect(770, 20, 70, 70), "" + _countPoint, _guiStyle);
    }
}
