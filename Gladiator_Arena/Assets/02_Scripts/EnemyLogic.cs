﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnemyState
{
    Spawning,
    Attacking,   
}
public class EnemyLogic : MonoBehaviour
{
    EnemyState _enemyState = EnemyState.Spawning;
    GameObject _Player;
    Vector2 _attackDirection;
    float _movementSpeed = 5.0f;
    Rigidbody2D _rigidbody;
    CountEnemyLogic _countEnemyLogic;
    public int _point;

    void Start()
    {
        _Player = GameObject.FindGameObjectWithTag("Player");
        _rigidbody = GetComponent<Rigidbody2D>();
        GameObject enemies = GameObject.Find("Count");
        _countEnemyLogic = enemies.GetComponent<CountEnemyLogic>();
    }
    private void FixedUpdate()
    {
        if (_enemyState == EnemyState.Attacking)
        {
          if(_rigidbody)
            {
                _rigidbody.MovePosition(_rigidbody.position + (_attackDirection * _movementSpeed * Time.deltaTime));
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        _attackDirection = -_attackDirection;
        Rigidbody2D rigidBody = collision.rigidbody;

        if(rigidBody&&rigidBody.tag=="Player")
        {
            PlayeLogic playerLogic = rigidBody.GetComponent<PlayeLogic>();

            if(playerLogic)
            {
                playerLogic.TakeDamage();
            }
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag=="PlayerAttack")
        {         
            Destroy(gameObject);            
            _countEnemyLogic._countPoint++;
        }
    }
    public void SetEnemyState(EnemyState enemyState)
    {
        _enemyState = enemyState;

        if(_enemyState==EnemyState.Attacking)
        {
            DetermineAttackDirection();
        }
    }
    void DetermineAttackDirection()
    {
        if(!_Player)
        {
            return;
        }

        _attackDirection = _Player.transform.position - transform.position;
        _attackDirection.Normalize();
    }
}
