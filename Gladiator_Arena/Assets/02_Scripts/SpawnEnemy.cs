﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemy : MonoBehaviour
{

    public GameObject[] _enemyPref;
    public Transform[] _spawnPoints;
    public float _minDelay = 1f;
    public float _maxDelay = 6f;

    void Start()
    {
        StartCoroutine(SpawnPoints());
        
    }
    IEnumerator SpawnPoints()
    {
        while(true)
        {
        float delay = Random.Range(_minDelay, _maxDelay);
        yield return new WaitForSeconds(delay);

        int spawnIndex = Random.Range(0, _spawnPoints.Length);
        Transform spawnPoint = _spawnPoints[spawnIndex];

        GameObject spawnedEnemy = Instantiate(_enemyPref[Random.Range(0, _enemyPref.Length)], spawnPoint.position, spawnPoint.rotation);

        

        }
    }
}
